// Chosen search to view search suggestions immediately to user
$(document).ready(
	function () {



        $("#country_id").chosen();
        $("#city_id").  chosen();
        $("#continent_id").chosen();
        $("#host_id").chosen();
        $("#sourcedb_id").chosen();

    });


$( function() {
		var dateFormat = "yy-mm-dd",
			from = $( "#from,#fromcreate,#fromupdate" )
				.datepicker({
					changeMonth: true,
                    dateFormat: "yy-mm-dd"
				})
				.on( "change", function() {
					to.datepicker( "option", "minDate", getDate( this ) );
				}),
			to = $( "#to,#tocreate,#toupdate" ).datepicker({
				changeMonth: true,
                dateFormat: "yy-mm-dd"
			})
			.on( "change", function() {
				from.datepicker( "option", "maxDate", getDate( this ) );
			});

		function getDate( element ) {
			var date;
			try {
				date = $.datepicker.parseDate( dateFormat, element.value );
			} catch( error ) {
				date = null;
			}

			return date;
		}
	} );
