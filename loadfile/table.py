from django_tables2.export.views import ExportMixin
import django_tables2 as tables
from .models import HEV


class TableView(ExportMixin, tables.Table):
    # selection = tables.CheckBoxColumn(accessor="pk", attrs={"th__input": {"onclick": "toggle(this)"}}, orderable=False, exclude_from_export=True)
    Genebank_ID = tables.TemplateColumn('<a target="_blank" href="https://www.ncbi.nlm.nih.gov/nuccore/{{record.Genebank_ID}}"><span>{{record.Genebank_ID}}</span></a>')
    # Title = tables.TemplateColumn('<a class="recordTitle" href="#"><span>{{record.Title}}</span></a>')
    Title = tables.TemplateColumn('<a href="javascript:void(0)" onclick="showAllData({{record.id}} , event)"><span>{{record.Title}}</span></a>',attrs = {"style":"text-align:left;"})
    collection_date = tables.TemplateColumn('<span>{{record.collection_date}}</span>')
    country = tables.TemplateColumn('<span>{{record.country}}</span>')
    City = tables.TemplateColumn('<span>{{record.City}}</span>')
    slen = tables.TemplateColumn('<span>{{record.slen}}</span>')
    genotype = tables.TemplateColumn('<span>{{record.genotype}}</span>')
    exclude_columns = ("buttons", )
    dataset_kwargs = {"title": "HEV"}
    action=tables.TemplateColumn("<a id='actionBtn' href='admin/loadfile/hev/{{record.id}}/change/'><button class='btn btn-danger'>Edit</button></a>",verbose_name = 'Actions')



# '<a target="_blank" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalScrollable href="">{{record.Title}}</a>'