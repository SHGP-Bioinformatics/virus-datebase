from django.shortcuts import render
from .models import HEV
from .table import TableView
from django_tables2.config import RequestConfig
from django_tables2.export.export import TableExport
import json
from django.http import HttpResponse
from django.forms.models import model_to_dict
import ast
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django_countries import countries
import pycountry

# Index Function ( View data as a table in home page & Search in DataBase and view result to user )

def index(request):
    kwargs = {}
    if request.GET.get('genebank_id', None):
        kwargs['Genebank_ID'] = request.GET['genebank_id']

    if request.GET.get('title', None):
        kwargs['Title__icontains'] = request.GET['title']

    if request.GET.get('country', 'Select') != 'Select':
        country = request.GET['country']
        if len(country.split()) < 3:
            kwargs['country__icontains'] = country
        elif len(country.split()) == 3:
            print(pycountry.countries.get(name=country).alpha_3)
            kwargs['country'] = pycountry.countries.get(name=country).alpha_3

    object = HEV.objects.filter(**kwargs)
    if not kwargs: obj = 0
    elif kwargs and not object: obj = 1
    else: obj = 2



    export_obj = TableView(object)
    RequestConfig(request).configure(export_obj)
    export_format = request.GET.get("_export", None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, export_obj)
        return exporter.response("Virus Database.{}".format(export_format))


    context = {
        'country_list': list(countries),
        'export_obj': export_obj,
        'object': object,
        'obj': obj,
    }


    return render(request, "loadfile/index.html", context)


def titlerecord(request):
    id = request.GET['id']
    object = HEV.objects.get(id=id)
    return HttpResponse(json.dumps({"object": model_to_dict(object)}))


def search(request):
    context = {}
    if request.method == 'GET':
        context['country_list'] = list(countries)
        context['req'] = 'GET'
    else:
        kwargs = request.POST.get('kwargs', {})
        if type(kwargs) == type("str"): kwargs = ast.literal_eval(kwargs)
        export_format = 'xlsx'
        if export_format in request.POST and TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, TableView(HEV.objects.filter(**kwargs)))
            return exporter.response("Virus Database.{}".format(export_format))
        else:
            if request.POST.get('genebank_id', None):
                kwargs['Genebank_ID'] = request.POST['genebank_id']

            if request.POST.get('version', None):
                kwargs['Version__icontains'] = request.POST['version']

            if request.POST.get('taxa_id', None):
                kwargs['Taxa_id'] = request.POST['taxa_id']

            if request.POST.get('selnstart', None) or request.POST.get('selnend', None):
                if request.POST.get('selnstart', None):
                    kwargs["slen__gte"] = request.POST['selnstart']
                if request.POST.get('selnend', None):
                    kwargs["slen__lte"] = request.POST['selnend']

            if request.POST.get('title', None):
                kwargs['Title__icontains'] = request.POST['title']

            if request.POST.get('collection_year_from', None) or request.POST.get('collection_year_to', None):
                if request.POST.get('collection_year_from', None):
                    date_from = request.POST['collection_year_from']
                    year_from = date_from[0:4]
                    kwargs['Year_of_Collection__gte'] = year_from
                if request.POST.get('collection_year_to', None):
                    date_to = request.POST['collection_year_to']
                    year_to = date_to[0:4]
                    kwargs['Year_of_Collection__lte'] = year_to
                kwargs['Year_of_Collection__NE'] = "NA"
                kwargs['Year_of_Collection__ne'] = "None"

            if request.POST.get('strain', None):
                kwargs['strain__icontains'] = request.POST['strain']

            if request.POST.get('isolate', None):
                kwargs['isolate__icontains'] = request.POST['isolate']

            if request.POST.get('create_date_from', None) or request.POST.get('create_date_to', None):
                if request.POST.get('create_date_from', None):
                    create_date_from = request.POST['create_date_from'] + ' 00:00:00'
                    kwargs['create_date__gte'] = create_date_from
                if request.POST.get('create_date_to', None):
                    create_date_to = request.POST['create_date_to'] + ' 00:00:00'
                    kwargs['create_date__lte'] = create_date_to
                kwargs['create_date__NE'] = "NA"
                kwargs['create_date__ne'] = "None"

            if request.POST.get('update_date_from', None) or request.POST.get('update_date_to', None):
                if request.POST.get('update_date_from', None):
                    update_date_from = request.POST['update_date_from'] + ' 00:00:00'
                    kwargs['update_date__gte'] = update_date_from
                if request.POST.get('update_date_to', None):
                    update_date_to = request.POST['update_date_to'] + ' 00:00:00'
                    kwargs['update_date__lte'] = update_date_to
                kwargs['update_date__NE'] = "NA"
                kwargs['update_date__ne'] = "None"

            if request.POST.get('continent', None):
                kwargs['Continent__icontains'] = request.POST['continent']

            if request.POST.get('country', 'Select') != 'Select':
                country = request.POST['country']
                if len(country.split()) < 3:
                    kwargs['country__icontains'] = country
                elif len(country.split()) == 3:
                    print(pycountry.countries.get(name=country).alpha_3)
                    kwargs['country'] = pycountry.countries.get(name=country).alpha_3

            if request.POST.get('city', None):
                kwargs['City__icontains'] = request.POST['city']


            if request.POST.get('host', None):
                kwargs['Host__icontains'] = request.POST['host']

            if request.POST.get('source_db', None):
                kwargs['source_db__icontains'] = request.POST['source_db']

            objects = HEV.objects.filter(**kwargs)
            if not kwargs: obj = False
            elif kwargs and not objects: obj = False
            else: obj = True

            page = str(request.POST.get('page', "?page=1")).split("?page=")[1]
            paginator = Paginator(objects, 25)
            try:
                object = paginator.page(page)
            except PageNotAnInteger:
                object = paginator.page(1)
            except EmptyPage:
                object = paginator.page(paginator.num_pages)
            export_obj = TableView(objects)
            export_obj.paginate(page=page, per_page=25)

            context['export_obj'] = export_obj
            context['object'] = object
            context['obj'] = obj
            context['req'] = 'POST'
            context['kwargs'] = kwargs
    return render(request, "loadfile/search.html", context)


# Assign function used to save the data which return from excel file to our django model in DataBase
def assign(result):
    obj = HEV()
    obj.Genebank_ID = result['Genebank ID']
    obj.Version = result['Version']
    obj.Title = result['Title']
    obj.create_date = result['create_date']
    obj.update_date = result['update_date']
    obj.Taxa_id = result['Taxa_id']
    obj.slen = result['slen']
    obj.source_db = result['source_db']
    obj.organism = result['organism']
    obj.strain = result['strain']
    obj.isolate = result['isolate']
    obj.country = result['country']
    obj.City = result['City']
    obj.Continent = result['Continent']
    obj.Host = result['Host']
    obj.Host_bis = result['Host-bis']
    obj.Year_of_Collection = result['Year of Collection']
    obj.collection_date = result['collection_date']
    obj.isolation_source = result['isolation_source']
    obj.genotype = result['genotype']
    obj.GLUE_genotype = result['GLUE genotype']
    obj.Pubmed_ID = result['Pubmed ID']
    obj.DOI = result['DOI']
    obj.Contact = result['Contact']
    obj.Sequencing_method = result['Sequencing method']
    obj.Comment = result['Comment']
    obj.type = result['type']
    obj.serogroup = result['serogroup']
    obj.acronym = result['acronym']
    obj.group = result['group']
    obj.clone_lib = result['clone_lib']
    obj.cell_type = result['cell_type']
    obj.pop_variant = result['pop_variant']
    obj.haplotype = result['haplotype']
    obj.sub_species = result['sub_species']
    obj.sub_clone = result['sub_clone']
    obj.old_lineage = result['old_lineage']
    obj.synonym = result['synonym']
    obj.dev_stage = result['dev_stage']
    obj.cell_line = result['cell_line']
    obj.sub_strain = result['sub_strain']
    obj.subgroup = result['subgroup']
    obj.metagenome_source = result['metagenome_source']
    obj.specimen_voucher = result['specimen_voucher']
    obj.altitude = result['altitude']
    obj.bio_material = result['bio_material']
    obj.sex = result['sex']
    obj.chromosome = result['chromosome']
    obj.culture_collection = result['culture_collection']
    obj.variety = result['variety']
    obj.serovar = result['serovar']
    obj.ecotype = result['ecotype']
    obj.common = result['common']
    obj.tissue_lib = result['tissue_lib']
    obj.gb_synonym = result['gb_synonym']
    obj.biotype = result['biotype']
    obj.save()
