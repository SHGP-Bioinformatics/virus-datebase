from django.conf.urls import include, url

from . import views

app_name = "loadfile"

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^search/', views.search, name='search'),
    url(r'^titlerecord/', views.titlerecord, name='titlerecord'),
]
