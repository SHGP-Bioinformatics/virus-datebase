from django.contrib import admin
from .models import HEV
from django.shortcuts import redirect


class HEVAdmin(admin.ModelAdmin):
    search_fields=['Genebank_ID','Title','country']
    list_filter =["country"]

    def save_model(self, request, obj, form, change):
        obj.save(request.user.username,"Editing From admin interface")
        # return redirect('loadfile:index')

    def response_post_save_change(self, request, obj):
        return redirect('loadfile:index')

    def get_form(self, request, obj = None, **kwargs):
        self.exclude = ("edited",)
        form = super(HEVAdmin, self).get_form(request, obj, **kwargs)
        return form

admin.site.register(HEV,HEVAdmin)
# Register your models here.
