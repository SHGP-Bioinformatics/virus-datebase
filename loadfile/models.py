from django.db import models
from django.db.models import Lookup
from django.db.models.fields import Field
from django_countries.fields import CountryField
from ModelTracker import Tracker

# Create your models here.

class HEV(Tracker.ModelTracker):
    Genebank_ID = models.CharField(max_length=255)
    Version = models.CharField(max_length=255)
    Title = models.CharField(max_length=255)
    create_date = models.CharField(max_length=255)
    update_date = models.CharField(max_length=255)
    Taxa_id = models.IntegerField(unique=False)
    slen = models.IntegerField()
    source_db = models.CharField(max_length=255)
    organism = models.CharField(max_length=255)
    strain = models.CharField(max_length=255)
    isolate = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    City = models.CharField(max_length=255)
    Continent = models.CharField(max_length=255)
    Host = models.CharField(max_length=255)
    Host_bis = models.CharField(max_length=255)
    Year_of_Collection = models.CharField(max_length=255)
    collection_date = models.CharField(max_length=255)
    isolation_source = models.CharField(max_length=255)
    genotype = models.CharField(max_length=255)
    GLUE_genotype = models.CharField(max_length=255)
    Pubmed_ID = models.CharField(max_length=255)
    DOI = models.CharField(max_length=255)
    Contact = models.CharField(max_length=255)
    Sequencing_method = models.CharField(max_length=255)
    Comment = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    serogroup = models.CharField(max_length=255)
    acronym = models.CharField(max_length=255)
    group = models.CharField(max_length=255)
    clone_lib = models.CharField(max_length=255)
    cell_type = models.CharField(max_length=255)
    pop_variant = models.CharField(max_length=255)
    haplotype = models.CharField(max_length=255)
    sub_species = models.CharField(max_length=255)
    sub_clone = models.CharField(max_length=255)
    old_lineage = models.CharField(max_length=255)
    synonym = models.CharField(max_length=255)
    dev_stage = models.CharField(max_length=255)
    cell_line = models.CharField(max_length=255)
    sub_strain = models.CharField(max_length=255)
    subgroup = models.CharField(max_length=255)
    metagenome_source = models.CharField(max_length=255)
    specimen_voucher = models.CharField(max_length=255)
    altitude = models.CharField(max_length=255)
    bio_material = models.CharField(max_length=255)
    sex = models.CharField(max_length=255)
    chromosome = models.CharField(max_length=255)
    culture_collection = models.CharField(max_length=255)
    variety = models.CharField(max_length=255)
    serovar = models.CharField(max_length=255)
    ecotype = models.CharField(max_length=255)
    common = models.CharField(max_length=255)
    tissue_lib = models.CharField(max_length=255)
    gb_synonym = models.CharField(max_length=255)
    biotype = models.CharField(max_length=255)
    edited=models.BooleanField(default = False,blank = True)

    def __unicode__(self):
        return self.Title

    def __str__(self):
        return self.Title

    class  Meta:
        verbose_name="HEV Record"
        verbose_name_plural = "HEV Records"



@Field.register_lookup
class NotEqualLookup(Lookup):
    lookup_name = 'ne'
    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s <> %s' % (lhs, rhs), params

@Field.register_lookup
class NELookup(Lookup):
    lookup_name = 'NE'
    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s <> %s' % (lhs, rhs), params
