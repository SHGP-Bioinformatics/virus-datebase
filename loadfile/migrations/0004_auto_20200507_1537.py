# Generated by Django 2.2 on 2020-05-07 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loadfile', '0003_auto_20200507_1535'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hev',
            name='Year_of_Collection',
            field=models.CharField(max_length=255),
        ),
    ]
