from django.shortcuts import render
from django.core.management.base import BaseCommand, CommandError
import openpyxl
from loadfile.models import HEV
from loadfile.views import assign


class Command(BaseCommand):
    help = 'load excel file'

    def add_arguments(self, parser):
        parser.add_argument('--file', nargs='+', type=str)

    def handle(self, *args, **options):
        # samples = open(options["file"][0]).read()
        # with open(options["file"][0], 'rb') as f:
        #     contents = f.read()

        # excel_file = request.FILES["excel_file"]
        wb = openpyxl.load_workbook(options["file"][0], 'rb')
        sheets = wb.sheetnames
        active_sheet = wb.active
        excel_data = list()
        # for sheet in sheets: ** To handle excel files with multi sheets
        worksheet = wb[sheets[0]]
        for row in worksheet.iter_rows():
            excel_data.append([str(cell.value) for cell in row])

        keys = [item for item in excel_data[0]]
        for item in excel_data[1:]:
            result = {}
            for i in range(0, len(keys)):
                result[keys[i]] = item[i]

            # Call function assign(dictionary) to save data from file to database
            assign(result)

        # print(excel_data)
        # return render(request, 'loadfile/index.html', {"excel_data":excel_data})









