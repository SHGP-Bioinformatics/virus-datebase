# Virus Database Run Guide

### You need Python >= 3.5 to run this system
 
 1. Download the lastest repo from [https://bitbucket.org/SHGP-Bioinformatics/virus-datebase/downloads/?tab=tags](https://bitbucket.org/SHGP-Bioinformatics/virus-datebase/downloads/?tab=tags)
 2. Extract the downloaded archive
 3. on the terminal, go to the extracted data
##### Install Virtualenv (if not installed)
    ~/virus-datebase$ python3 -m pip install virtualenv 
##### Create your venv using this command:
    ~/virus-datebase$ virtualenv --python python3 env  
##### Activate the virtualenv
    ~/virus-datebase$ source env/bin/activate    
#### Install requirements
    ~/virus-datebase$ pip install -r requirements.txt
##### Run server to use site features (search in Database, export search result as excel file) 
    ~/virus-datebase$ python manage.py runserver

Open a browser (Firefox or Chrome) and go to "[http://localhost:8000/](http://localhost:8000/)"
    

